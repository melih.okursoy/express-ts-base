FROM node:alpine as builder
COPY package*.json ./
RUN npm install
COPY . .
RUN npm install && npm run build 


FROM node:alpine
ENV PORT=8080
WORKDIR /usr/src/app
COPY --from=builder package*.json ./
RUN npm ci --only=production
COPY  --from=builder ./dist .
EXPOSE ${PORT}
CMD [ "node", "index.js" ]