import express, { Application } from 'express';
import bodyParser from 'body-parser';
import cors from "cors";

const app: Application = express();
app.use(bodyParser.json());
app.use(cors());

function loggerMiddleware(request: express.Request, response: express.Response, next: express.NextFunction) {
  console.log(`${request.method} ${request.path}`);
  next();
}

app.use(loggerMiddleware);


app.get("/", (req: express.Request, res: express.Response) => {
  res
    .status(200)
    .send("Express + TypeScript Server");
});

const PORT = process.env.PORT || 8000;
app.listen(PORT, () => {
  console.log(`⚡️[server]: Server is running at https://localhost:${PORT}`);
});

export default app;
